from django.contrib import admin

# Register your models here.


from .models import Like
from .models import Commentary
from .models import Place
from .models import Profile
from .models import Post
from .models import Notification
from .models import Youtube
from .models import PushNotification
from .models import Reported



class LikeAdmin(admin.ModelAdmin):
    list_display = ['id','post','date' , 'profile'  ]
    list_filter = ('profile', )



admin.site.register(Like , LikeAdmin)



class CommentaryAdmin(admin.ModelAdmin):
    list_display = ['id','text','date' , 'user' , 'post' ]
    list_filter = ('user', )


admin.site.register(Commentary , CommentaryAdmin)



class PlaceAdmin(admin.ModelAdmin):
    list_display = ['id','name','located_in' ]

admin.site.register(Place, PlaceAdmin)



class ProfileAdmin(admin.ModelAdmin):
    list_display = ['id','user','phone','url_image_profile' ]

admin.site.register(Profile, ProfileAdmin)

class PostAdmin(admin.ModelAdmin):
    list_display = ['id','type_post','place','user' , 'likes', 'state']
    list_filter = ('type_post','user' , 'place' , 'state')

admin.site.register(Post,PostAdmin)


class NotificationAdmin(admin.ModelAdmin):
    list_display = ['id','type_notification','profile','post' , 'date']
    list_filter = ('type_notification','profile' , 'post')

admin.site.register(Notification ,NotificationAdmin )



class YoutubeAdmin(admin.ModelAdmin):
    list_display = ['id','access_token','client_id','client_secret' , 'refresh_token']

admin.site.register(Youtube ,  YoutubeAdmin)

class PushAdmin(admin.ModelAdmin):
    list_display = ['id','type_push','user','instance_id' , 'date']
    list_filter = ('type_push','user' )

admin.site.register(PushNotification , PushAdmin)

class ReportedAdmin(admin.ModelAdmin):
    list_display = ['id','user_report','post_reported' ]
    list_filter = ('user_report','post_reported' )

admin.site.register(Reported, ReportedAdmin)



