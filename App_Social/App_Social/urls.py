"""App_Social URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from rest_framework import routers
from Api import views
from django.conf.urls.static import static
from django.conf import settings
from rest_framework.authtoken import views as authviews



router = routers.DefaultRouter()
# Post  publicaciones ,   
router.register(r'wallall', views.WallAllViewSet)
router.register(r'vr', views.VRViewSet)
# Get  publicaciones ,   
router.register(r'wallallget', views.WallAllGetViewSet)
router.register(r'vrget', views.VRGetViewSet)

# Post Commentary
router.register(r'Comments', views.CommentsViewSet)

router.register(r'profile', views.ProfileViewSet)

router.register(r'credentialyoutube', views.CredentialYoutubeViewSet)

#router.register(r'credentialyoutubeios', views.CredentialYoutubeiosViewSet)



router.register(r'profileupdate', views.ProfileUpdateViewSet)
router.register(r'profileupdate2', views.ProfileUpdate2ViewSet)



router.register(r'passwordchange', views.PasswordChangeViewSet)

router.register(r'report', views.ReportViewSet)

router.register(r'push', views.PushViewSet)



# get profile 

#router.register(r'profileget', views.ProfileGetViewSet)

router.register(r'likes', views.LikesViewSet)

router.register(r'places', views.PlacesViewSet)




urlpatterns = [
    
    url(r'^$', views.index),
 
    url(r'^api/token-auth/', authviews.obtain_auth_token),

    url(r'^app/v1/api/', include(router.urls)),


    url(r'^admin/', admin.site.urls),



    # get Commentary


    #url(r'^youtube/', views.youtube ),



    url(r'^recuperarpassword/', views.recuperarpassword),
    # url(r'^enviaremail/', views.enviaremail),


    





    url(r'^getcommentary/(?P<post>.+)/$', views.GetComments.as_view()),

    url(r'^api/detailprofile/', views.DetailProfileViewSet.as_view()),


    #url(r'^app/v1/api/profileupdate/', views.ProfileUpdateViewSet.as_view()),



    url(r'^app/v1/api/filterprofile/(?P<id>.+)/$', views.FilterProfileViewSet.as_view()),
    url(r'^app/v1/api/getprofile/', views.ProfileGetViewSet.as_view()),
    url(r'^app/v1/api/notifications/', views.NotificationsViewSet.as_view()),
    


    url(r'^', include('Administrator.urls')),

    url(r'^', include('Api.urls')),

    # url(r'^.*$', views.index),



    # url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))

]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)



handler404 = 'views.index'

