# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-05-01 23:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Api', '0020_auto_20170501_0137'),
    ]

    operations = [
        migrations.CreateModel(
            name='youtube',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('access_token', models.CharField(max_length=2000)),
                ('expires_in', models.CharField(max_length=2000)),
                ('client_id', models.CharField(max_length=2000)),
                ('client_secret', models.CharField(max_length=2000)),
            ],
        ),
    ]
