# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-05-02 02:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Api', '0022_remove_youtube_expires_in'),
    ]

    operations = [
        migrations.AddField(
            model_name='youtube',
            name='refresh_token',
            field=models.CharField(blank=True, max_length=2000, null=True),
        ),
    ]
