# from django.shortcuts import render

from django.contrib.auth.models import User, Group
from rest_framework import viewsets,generics
from rest_framework.permissions import IsAuthenticated
from .models import *
from .serializers import *
from rest_framework import generics
from django.http import HttpResponse,HttpResponseRedirect
import json
from django.shortcuts import render
from rest_framework.pagination import PageNumberPagination


from oauth2client import client

from django.http import HttpResponse


def index(request):
    return render(request, 'index.html', locals())



def recuperarpassword(request):
    
    return render(request, 'recuperarpassword.html', locals())


def gooauth(request):

    flow = client.flow_from_clientsecrets(
        '/media_django/client_secrets.json',
        scope='https://www.googleapis.com/auth/youtube.upload',
        redirect_uri='https://www.boyacaen360.com/app/v1/api/gooauth')
    flow.params['access_type'] = 'offline'         # offline access
    flow.params['include_granted_scopes'] = 'true'   # incremental auth

    if not request.GET.__contains__('code'):
     
        return HttpResponseRedirect(flow.step1_get_authorize_url())

    else:
        auth_code = request.GET['code']

        credentials = flow.step2_exchange(auth_code)
        test =credentials.to_json()
        
        t = json.loads(test)

        actualizar , creado  = Youtube.objects.get_or_create(pk=1, defaults = { 'access_token' : t['access_token'] , 'client_id' : t['client_id'] , 'client_secret': t['client_id'] , 'refresh_token' : t['refresh_token']  })
        actualizar.access_token = t['access_token']
        actualizar.client_id = t['client_id']
        actualizar.client_secret = t ['client_secret']
        actualizar.refresh_token = t ['refresh_token']

        actualizar.save()

        #credential = Youtube(access_token = t['access_token'] , client_id= t['client_id'] , client_secret = t['client_secret'] )
        #credential.save()

        return HttpResponseRedirect('/')
    

from oauth2client import client, GOOGLE_TOKEN_URI, GOOGLE_REVOKE_URI

import httplib2


def refreshYoutube(request):
    token  =  Youtube.objects.get(pk = 1)

    CLIENT_ID = token.client_id
    CLIENT_SECRET = token.client_secret
    REFRESH_TOKEN = token.refresh_token

    credentials = client.OAuth2Credentials(
    None, CLIENT_ID, CLIENT_SECRET, REFRESH_TOKEN, None, GOOGLE_TOKEN_URI,
    None, revoke_uri=GOOGLE_REVOKE_URI)

    # refresh the access token (or just try using the service)
    credentials.refresh(httplib2.Http())

    test =credentials.to_json()
        
    t = json.loads(test)

    actualizar , creado  = Youtube.objects.get_or_create(pk=1, defaults = { 'access_token' : t['access_token'] , 'client_id' : t['client_id'] , 'client_secret': t['client_id'] , 'refresh_token' : t['refresh_token']  })
    actualizar.access_token = t['access_token']
    actualizar.client_id = t['client_id']
    actualizar.client_secret = t ['client_secret']
    actualizar.refresh_token = t ['refresh_token']

    actualizar.save()
 
    return HttpResponseRedirect('/app/v1/api/credentialyoutube/?format=json')


class CredentialYoutubeViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated,)
    queryset = Youtube.objects.all()
    serializer_class = CredentialYoutubeSerializer
    http_method_names = [ 'get', 'head' , 'options']


class CredentialYoutubeiosViewSet(viewsets.ModelViewSet):
    print('---------- esto se ejecuto------------')
    # refreshYoutube(request)

    permission_classes = (IsAuthenticated,)
    queryset = Youtube.objects.all()
    serializer_class = CredentialYoutubeiosSerializer
    http_method_names = [ 'get', 'head' , 'options']


     
class WallAllGetViewSet(viewsets.ModelViewSet):
    
    permission_classes = (IsAuthenticated,)
    queryset = Post.objects.all().order_by('-date')
    serializer_class = WallAllGetSerializer
    http_method_names = [ 'get', 'head' , 'options']


class VRGetViewSet(viewsets.ModelViewSet):
   

    permission_classes = (IsAuthenticated,)
    queryset = Post.objects.filter(type_post__in=['P3','V3']).order_by('-date')
    serializer_class = VRGetSerializer
    http_method_names = [ 'get', 'head' , 'options']


 
class WallAllViewSet(viewsets.ModelViewSet):
    
    permission_classes = (IsAuthenticated,)
    queryset = Post.objects.all().order_by('-date')
    serializer_class = WallAllSerializer

    def perform_create(self, serializer):
        profile = Profile.objects.get(user__pk=self.request.user.pk)
        serializer.save(user= profile )

class VRViewSet(viewsets.ModelViewSet):
   

    permission_classes = (IsAuthenticated,)
    queryset = Post.objects.filter(type_post__in=['P3','V3']).order_by('-date')
    serializer_class = VRSerializer
    
    def perform_create(self, serializer):
        profile = Profile.objects.get(user__pk=self.request.user.pk)
        serializer.save(user= profile )

"""
Comentarios por filtro url  solo GET
"""   
 
class GetComments(generics.ListAPIView):
    
    serializer_class = CommentsSerializer

    def get_queryset(self):
        
        post =   self.kwargs['post'] 
        return Commentary.objects.filter(post=post)



 

class FilterProfileViewSet(generics.ListAPIView):
     
    permission_classes = (IsAuthenticated,)
    # queryset = Profile.objects.all()
    serializer_class = ProfileSerializerRead
    #  SELECCIONA QUE METODOS ESTAN DISPONIBLES
    # http_method_names = [ 'get', 'head']

    def get_queryset(self):

        return Profile.objects.filter(pk=self.kwargs['id'])
 

class CommentsViewSet(viewsets.ModelViewSet):
    
    permission_classes = (IsAuthenticated,)
    queryset = Commentary.objects.all()
    serializer_class = CommentsSerializer

    def perform_create(self, serializer):
        profile = Profile.objects.get(user__pk=self.request.user.pk)
        serializer.save(user= profile )

 

class ReportViewSet(viewsets.ModelViewSet):
    
    permission_classes = (IsAuthenticated,)
    queryset = Reported.objects.all()
    serializer_class = ReportSerializer

    def perform_create(self, serializer):
        profile = Profile.objects.get(user__pk=self.request.user.pk)
        serializer.save(user= profile )


class PushViewSet(viewsets.ModelViewSet):
    
    permission_classes = (IsAuthenticated,)
    queryset = PushNotification.objects.all()
    serializer_class = PushSerializer

    def perform_create(self, serializer):
        profile = Profile.objects.get(user__pk=self.request.user.pk)
        serializer.save(user= profile )




class ProfileViewSet(viewsets.ModelViewSet):
    
    # permission_classes = (IsAuthenticated,)
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    #  SELECCIONA QUE METODOS ESTAN DISPONIBLES
    http_method_names = [ 'post', 'head' , 'options']



class ProfileUpdate2ViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated,)
    queryset = Profile.objects.all()
    serializer_class = ProfileUpdate2Serializer
    #  SELECCIONA QUE METODOS ESTAN DISPONIBLES
    #http_method_names = [ 'put', 'head' , 'options']

class ProfileUpdateViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated,)
    queryset = Profile.objects.all()
    serializer_class = ProfileUpdateSerializer
    #  SELECCIONA QUE METODOS ESTAN DISPONIBLES
    #http_method_names = [ 'put', 'head' , 'options']


class PasswordChangeViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated,)
    queryset = User.objects.all()
    serializer_class = PasswordChangeSerializer

    
    




class ProfileGetViewSet(generics.ListAPIView):
    
    permission_classes = (IsAuthenticated,)
    
    serializer_class = ProfileGetSerializer

    def get_queryset(self):

        user = self.request.user
        return Profile.objects.filter(user=user)



class DetailProfileViewSet(generics.ListAPIView):
     
    permission_classes = (IsAuthenticated,)
    # queryset = Profile.objects.all()
    serializer_class = ProfileSerializerRead
    #  SELECCIONA QUE METODOS ESTAN DISPONIBLES
    # http_method_names = [ 'get', 'head']

    def get_queryset(self):

        user = self.request.user
        return Profile.objects.filter(user=user)


class LikesViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    
    permission_classes = (IsAuthenticated,)
    queryset = Like.objects.all()
    serializer_class = LikeSerializer

    def perform_create(self, serializer):
        profile = Profile.objects.get(user__pk=self.request.user.pk)
        serializer.save(profile= profile )



class LargeResultsSetPagination(PageNumberPagination):
    page_size = 1000
    page_size_query_param = 'page_size'
    max_page_size = 10000
 

class PlacesViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    
    permission_classes = (IsAuthenticated,)
    queryset = Place.objects.all()
    serializer_class = PlaceSerializer
    pagination_class = LargeResultsSetPagination


class NotificationsViewSet(generics.ListAPIView):
      
    permission_classes = (IsAuthenticated,)

    serializer_class = NotificationSerializer

    def get_queryset(self):

        user = self.request.user
        post = Post.objects.filter(user = Profile.objects.get(user=user))
        return Notification.objects.filter(post__in=post)

def view_404(request):
    # make a redirect to homepage
    # you can use the name of url or just the plain link
    return redirect('/') # or redirect('name-of-index-url')






 
