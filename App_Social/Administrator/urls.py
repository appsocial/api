from django.conf.urls import url,include


from .import views


urlpatterns = [
    # url(r'^index/', views.index, name='index'),
    url(r'^administrador/', views.ingresar,name='ingresar'),
    url(r'^panelAdmin/', views.panelAdmin,name='panelAdmin'),
    url(r'^panelAdminFiltro/(?P<idLugar>\d+)/', views.panelAdminFiltro,name='panelAdminFiltro'),


    url(r'^usuariosAdmin/', views.usuariosAdmin,name='usuariosAdmin'),
    url(r'^detalleusuario/(?P<idUsuario>\d+)/', views.detalleusuario,name='detalleusuario'),


    url(r'^publicarAdmin/', views.formularioPublicar,name='publicarAdmin'),

    url(r'^guardarFoto/', views.guardarFoto,name='guardarFoto'), 
    url(r'^guardarVideo/', views.guardarVideo,name='guardarVideo'),

    url(r'^reportadoAdmin/', views.reportadoAdmin,name='reportadoAdmin'),



    

    url(r'^eliminarPublicacion/(?P<idPublicacion>\d+)/', views.eliminarPublicacion,name='eliminarPublicacion'), 


    url(r'^eliminarUsuario/(?P<idUsuario>\d+)/', views.eliminarUsuario,name='eliminarUsuario'), 

    url(r'^eliminarPost/(?P<idPost>\d+)/$', views.eliminarPost,name='eliminarPost'), 






    
    url(r'^baseadmin/', views.baseadmin,name='baseadmin'),




       #Login usuarios
    url(r'^logearse/',views.logearse,name='logearse'),
    url(r'^preguntar/',views.preguntar,name='preguntar'),

    url(r'^salir/',views.salir,name='salir'),




]