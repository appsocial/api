from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
 

# Create your models here.

from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.conf import settings



from PIL import Image as Img
import StringIO
from django.core.files.uploadedfile import InMemoryUploadedFile



@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class  Place(models.Model):
	name = models.CharField(max_length=100)
	located_in = models.ForeignKey('self', blank=True, null=True)
	def __unicode__(self):
		return self.name


class Profile(models.Model):
	user = models.OneToOneField(User,related_name='users')
	phone = models.CharField(max_length=30)
	id_facebook= models.CharField(max_length=400 , blank=True, null=True)
	id_instagram= models.CharField(max_length=400 , blank=True, null=True)
	id_google= models.CharField(max_length=400 , blank=True, null=True)
	# birth_place = models.ForeignKey(Place,blank=True, null=True,related_name='lugar')
	url_image_profile = models.ImageField(upload_to='Images/Profile/',blank=True,null=True,default="profile.jpg")
	
	def token(self):
		try: 
			t = Token.objects.filter(user_id=self.user.pk).values('key')
		except:
			t = 0
		return t

	def save(self, *args, **kwargs):
		if self.url_image_profile:
			img = Img.open(StringIO.StringIO(self.url_image_profile.read()))
			if img.mode != 'RGB':
				img = img.convert('RGB')
			img.thumbnail((self.url_image_profile.width/1.5,self.url_image_profile.height/1.5), Img.ANTIALIAS)
			output = StringIO.StringIO()
			img.save(output, format='JPEG', quality=70)
			output.seek(0)
			self.url_image_profile= InMemoryUploadedFile(output,'ImageField', "%s.jpg" %self.url_image_profile.name.split('.')[0], 'image/jpeg', output.len, None)
		super(Profile, self).save(*args, **kwargs)

	def __unicode__(self):
		return self.user.username
		

class Post(models.Model):
	type_post_id = (
        ('PH', 'Photo'),
        ('VI', 'Video'),
        ('P3', 'Photo 360'),
        ('V3', 'Video 360'))
	type_state = (
		('P', 'Public'),
		('C', 'Censored'))
	description = models.CharField(max_length=200)
	url_image = models.ImageField(upload_to='Images/',blank=True,null=True,default="/media/profile.jpg")
	url_image_thumbnail = models.ImageField(upload_to='Images/Thumbnail/',blank=True,null=True,default="/media/profile.jpg")
	url_video = models.CharField(max_length=400,blank=True,null=True)
	type_post = models.CharField(max_length=2,choices=type_post_id)
	state = models.CharField(max_length=1,choices=type_state)
	place = models.ForeignKey(Place)
	user = models.ForeignKey(Profile,blank=False,related_name='posts')
	date = models.DateTimeField(auto_now_add=True)
	likes = models.ManyToManyField(Profile,through='Like',through_fields=('post', 'profile'))

	def __unicode__(self):
	    return self.description

	def likes(self):
		return Like.objects.filter(post=self).count()

	def number_of_comments(self):
		return Commentary.objects.filter(post=self).count()
	
	def save(self, *args, **kwargs):
		if self.type_post == 'PH' or self.type_post == 'P3':
			if self.url_image:
				img = Img.open(StringIO.StringIO(self.url_image.read()))
				if img.mode != 'RGB':
					img = img.convert('RGB')
				img.thumbnail((self.url_image.width/1.5,self.url_image.height/1.5), Img.ANTIALIAS)
				output = StringIO.StringIO()
				img.save(output, format='JPEG', quality=70)
				output.seek(0)
				self.url_image_thumbnail= InMemoryUploadedFile(output,'ImageField', "%s.jpg" %self.url_image.name.split('.')[0], 'image/jpeg', output.len, None)
		super(Post, self).save(*args, **kwargs)

 	
# class Post(models.Model):
	# type_post_id = (
 #        ('PH', 'Photo'),
 #        ('VI', 'Video'),
 #        ('P3', 'Photo 360'),
 #        ('V3', 'Video 360'))
	# description = models.CharField(max_length=200)
	# url_image = models.ImageField(upload_to='Images/',blank=True,null=True)
	# url_video = models.CharField(max_length=400,blank=True,null=True)
	# type_post = models.CharField(max_length=2,choices=type_post_id)
	# state = models.CharField(max_length=10)
	# place = models.ForeignKey(Place)
	# user = models.ForeignKey(Profile,blank=False)
	# date = models.DateTimeField(auto_now_add=True)

	# def __str__(self):
	#     return self.description

	# def likes(self):
	# 	return Like.objects.filter(post=self).count()

	# def number_of_comments(self):
	# 	return Commentary.objects.filter(post=self).count()



class  Like(models.Model):
	date = models.DateTimeField(auto_now_add=True)
	post = models.ForeignKey(Post,blank=False)
	profile = models.ForeignKey(Profile,blank=False)
	def __unicode__(self):
		return self.profile.user.username

# class  Like(models.Model):
# 	date = models.DateTimeField(auto_now_add=True)
# 	post = models.ForeignKey(Post,blank=False)
# 	user = models.ForeignKey(Profile,blank=False)
# 	def __str__(self):
# 		return self.user.user.username


class Commentary(models.Model):
	text = models.CharField(max_length=400)
	date = models.DateTimeField(auto_now_add=True)
	user = models.ForeignKey(Profile,blank=False ,related_name='comment')
	post = models.ForeignKey(Post,blank=False,related_name='comments')
	def __unicode__(self):
		return self.text


class Notification(models.Model):
	type_notification_id = (
        ('C', 'Comentario'),
        ('L', 'Like'),
        ('P', 'Nueva Publicacion'))
	type_notification = models.CharField(max_length=1,choices=type_notification_id)
	profile = models.ForeignKey(Profile,blank=False,related_name='send')
	post = models.ForeignKey(Post,blank=False)
	date = models.DateTimeField(auto_now_add=True)
	
class Youtube(models.Model):
	access_token = models.CharField(max_length=2000)
	client_id= models.CharField(max_length=2000)
	client_secret =models.CharField(max_length=2000)
	refresh_token = models.CharField(max_length=2000,blank=True,null=True)
		

class Reported(models.Model):
	user_report = models.ForeignKey(Profile,blank=False ,related_name='report')
	post_reported = models.ForeignKey(Post,blank=False,related_name='reported')	


class PushNotification(models.Model):
	type_push_id = (
        ('I', 'iOS'),
        ('A', 'Android') )
	type_push = models.CharField(max_length=1,choices=type_push_id,null=True)
	user = models.ForeignKey(Profile,blank=False ,related_name='notify')
	instance_id = models.CharField(max_length=2000 , null=False , blank= False)
	date = models.DateTimeField(auto_now_add=True)


		










