from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import *


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField( style={'input_type': 'password'} ,write_only=True)

    class Meta:
        model = User
        fields = ('username', 'id' ,'password', 'email' , 'first_name' , 'last_name')

 




class ProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    phone = serializers.CharField(max_length=30,allow_blank=True)
    #url_image_profile = serializers.CharField(max_length=400,default="/images/profile.jpg")

    class Meta:
        model = Profile
        fields = ('url' , 'phone' , 'id' , 'id_facebook', 'user' , 'token')
        # fields = ('__all__')

    def create(self, validated_data):
        # album = User.objects.create(**validated_data['user'])
        tmp = validated_data['user']
        user = User(email=tmp['email'], username=tmp['username'],first_name=tmp['first_name'],last_name=tmp['last_name'])
        user.set_password(tmp['password'])
        user.save()
        del validated_data['user'] 
        return Profile.objects.create(user=user,**validated_data )


    def update(self, instance, validated_data):
        tmp = validated_data
        del tmp['user']
        self.create_or_update_profile(instance, tmp)
        return super(UserSerializer, self).update(instance, validated_data['user'])

    def create_or_update_profile(self, user, profile_data):
        profile, created = Profile.objects.get_or_create(user=user, defaults=profile_data)
        if not created and profile_data is not None:
            super(UserSerializer, self).update(profile, profile_data)

 

class UserUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ( 'id' , 'email' , 'first_name' , 'last_name')

 


class ProfileUpdate2Serializer(serializers.ModelSerializer):

    class Meta:
        model = Profile
        fields = ('url' , 'phone' , 'url_image_profile' , 'id' , 'id_facebook' )
  
    def update(self, instance, validated_data):

        if validated_data['phone']:
            instance.phone = validated_data['phone']
        if validated_data['id_facebook']:
            instance.id_facebook = validated_data['id_facebook']

        if validated_data['url_image_profile']:
            instance.url_image_profile = validated_data['url_image_profile']
        instance.save() 

        return instance 


class ProfileUpdateSerializer(serializers.ModelSerializer):
    user = UserUpdateSerializer()
    phone = serializers.CharField(max_length=30,allow_blank=True)

    class Meta:
        model = Profile
        fields = ('url' , 'phone' , 'url_image_profile' , 'id' , 'id_facebook', 'user' )
        # fields = ('__all__')

  
    def update(self, instance, validated_data):
        
        user = validated_data['user']

        instanceProfile = User.objects.get(email= user['email'])


        instanceProfile.first_name = user['first_name']
        instanceProfile.last_name = user['last_name']
        instanceProfile.save()

        profileInstance = Profile.objects.get(user=instanceProfile)

        profileInstance.phone = validated_data['phone']
        profileInstance.id_facebook = validated_data['id_facebook']

        if validated_data['url_image_profile']:
            profileInstance.url_image_profile = validated_data['url_image_profile']
        profileInstance.save() 

        return profileInstance 

from rest_framework.exceptions import APIException


class PasswordChangeSerializer(serializers.ModelSerializer):
    new_password = serializers.CharField( style={'input_type': 'password'} ,write_only=True)
    old_password = serializers.CharField( style={'input_type': 'password'} ,write_only=True)


    class Meta:
        model = User
        fields = ('url'   , 'id' , 'email', 'old_password'  ,'new_password')
        # fields = ('__all__')

  
    def update(self, instance, validated_data):

        userInstance = User.objects.get(email= validated_data['email'])

        if not userInstance.check_password(validated_data['old_password']):

            
            raise APIException("El password no coincide")

        else:
            userInstance.set_password(validated_data['new_password'])


        userInstance.save()



        return userInstance


class WallAllNoUserSerializer(serializers.ModelSerializer):
    # url_image = serializers.ImageField(max_length=None, allow_empty_file=True, use_url=True)
    class Meta:
        model = Post
        #fields = ('url','id','description', 'user' ,  'url_image', 'url_video','type_post', 'state', 'place'  , 'date' , 'likes' , 'number_of_comments' )
        fields = ('__all__')


class ProfileSerializerReadNoUser(serializers.ModelSerializer):
    user = UserSerializer()
    phone = serializers.CharField(max_length=30,allow_blank=True)
    #url_image_profile = serializers.CharField(max_length=400,default="/images/profile.jpg")

    class Meta:
        model = Profile
        fields = ('url' , 'id' ,  'phone' , 'url_image_profile' , 'id_facebook', 'user', 'posts' )
        # fields = ('__all__')



class CommentsNoUserSerializer(serializers.ModelSerializer):


    class Meta:
        model = Commentary
        fields = ( '__all__')



class WallAllSerializer(serializers.ModelSerializer):
    user = ProfileSerializerReadNoUser(read_only=True)
    comments = CommentsNoUserSerializer(many=True,read_only=True)
    # url_image = serializers.ImageField(max_length=None, allow_empty_file=True, use_url=True)
    class Meta:
        model = Post
        fields = ('url','id','description', 'user' ,  'url_image', 'url_video','type_post', 'state', 'place' , 'user' , 'date' , 'likes' , 'number_of_comments', 'comments' )





class ProfileSerializerRead(serializers.ModelSerializer):
    user = UserSerializer()
    phone = serializers.CharField(max_length=30,allow_blank=True)
    #url_image_profile = serializers.CharField(max_length=400,default="/images/profile.jpg")

    posts  = WallAllSerializer(many=True,read_only=True)
    

    class Meta:
        model = Profile
        fields = ('url' , 'id' ,  'phone' , 'url_image_profile' , 'id_facebook', 'user', 'posts' )
        # fields = ('__all__')





class CommentsSerializer(serializers.ModelSerializer):

    user = ProfileSerializerRead(read_only=True)

    class Meta:
        model = Commentary
        # fields = ( 'url' ,  'id','text', 'user', 'post' , 'date' , 'comment'  )
        fields = ( '__all__')

    def create(self, validated_data):
        commentary = Commentary.objects.create(text= validated_data['text'] ,post=validated_data['post'], user = validated_data['user'] )
        Notification.objects.create( type_notification='C' , post  = validated_data['post'] , profile =validated_data['user'] )

        return commentary



        #Notification.objects.create( type_notification='C' , post  = validated_data['post'] , profile =validated_data['user'] )
class ReportSerializer(serializers.ModelSerializer):

    user_report = ProfileSerializerRead(read_only=True)

    class Meta:
        model = Reported
        # fields = ( 'url' ,  'id','text', 'user', 'post' , 'date' , 'comment'  )
        fields = ( '__all__')

    def create(self, validated_data):
        report = Reported.objects.create(post_reported=validated_data['post_reported'],user_report = validated_data['user'])
        #reportar notificacion pagina web 
        return report

class PushSerializer(serializers.ModelSerializer):

    user = ProfileSerializerRead(read_only=True)

    class Meta:
        model = PushNotification
        # fields = ( 'url' ,  'id','text', 'user', 'post' , 'date' , 'comment'  )
        fields = ( '__all__')

    def create(self, validated_data):
        push = PushNotification.objects.create(instance_id=validated_data['instance_id'],user = validated_data['user'], type_push = validated_data['type_push'] )
        #reportar notificacion pagina web 
        return push

class LikeSerializer(serializers.ModelSerializer):

    profile = ProfileSerializerRead(read_only=True)

    class Meta:
        model = Like
        # fields = ( 'url' ,'user','post' )
        fields = ( '__all__')

    def create(self, validated_data):
        like = Like.objects.create(**validated_data )

        return Notification.objects.create( type_notification='L' , **validated_data  )




class PlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Place
        #fields = ('url','id','description', 'user' , 'url_image', 'url_video','type_post', 'state', 'place' , 'user' , 'date' ,'likes' ,  'number_of_comments' , 'comments')
        fields = ( '__all__')





class VRSerializer(serializers.ModelSerializer):
    
    user = ProfileSerializerRead(read_only=True)
    comments = CommentsSerializer(many=True,read_only=True)
    
    class Meta:

		model = Post
		fields = ('url','id','description', 'user' , 'url_image', 'url_video','type_post', 'state', 'place' , 'user' , 'date' ,'likes' ,  'number_of_comments' , 'comments')


class WallAllGetSerializer(serializers.ModelSerializer):
    user = ProfileSerializerRead(read_only=True)

    place = PlaceSerializer(read_only=True)

    comments = CommentsSerializer(many=True,read_only=True)
    class Meta:
        model = Post
        fields = ('url','id','description', 'user' ,  'url_image', 'url_image_thumbnail' ,'url_video','type_post', 'state', 'place' , 'user' , 'date' , 'likes' , 'number_of_comments', 'comments' )

class VRGetSerializer(serializers.ModelSerializer):
    place = PlaceSerializer(read_only=True)
    
    user = ProfileSerializerRead(read_only=True)
    class Meta:



        model = Post
        fields = ('url','id','description', 'user' , 'url_image', 'url_image_thumbnail' ,  'url_video','type_post', 'state', 'place' , 'user' , 'date' ,'likes' ,  'number_of_comments' , 'comments')



class ProfileGetSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    phone = serializers.CharField(max_length=30,allow_blank=True)
    #url_image_profile = serializers.CharField(max_length=400,default="/images/profile.jpg")

    posts  = WallAllSerializer(many=True,read_only=True)


    class Meta:
        model = Profile
        #fields = ('url' , 'phone' , 'id', 'url_image_profile' , 'id_facebook', 'user' , 'token')
        fields = ('__all__')
 



class NotificationSerializer(serializers.ModelSerializer):
    profile = ProfileSerializerRead(read_only=True)
    id_post = serializers.CharField(source='post.id')

    class Meta:
        model = Notification
        #fields = ('url','id','description', 'user' , 'url_image', 'url_video','type_post', 'state', 'place' , 'user' , 'date' ,'likes' ,  'number_of_comments' , 'comments')
        fields = ( 'id_post' , 'id' , 'profile', 'type_notification')




class CredentialYoutubeSerializer(serializers.ModelSerializer):

    

    class Meta:
        model = Youtube
        #fields = ('url' , 'phone' , 'id', 'url_image_profile' , 'id_facebook', 'user' , 'token')
        fields = ('__all__')

from oauth2client import client, GOOGLE_TOKEN_URI, GOOGLE_REVOKE_URI

import httplib2

class CredentialYoutubeiosSerializer(serializers.ModelSerializer):

     

    class Meta:
        model = Youtube
        #fields = ('url' , 'phone' , 'id', 'url_image_profile' , 'id_facebook', 'user' , 'token')
        fields = ('__all__')
 

      