from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, authenticate, logout
from django.http import HttpResponse,HttpResponseRedirect
from Api.models import *






# Create your views here.



'''
Autor 			Jhonatan Acelas Arevalo
Fecha 	 		11 Julio 2015
Descripcion  	Pregunta si el usuario esta logueado y envia a ingresar o preguntar tipo usuario
Funcion 		Gestion.1
'''
def home(request):
	if not request.user.is_anonymous():
		return HttpResponseRedirect('/preguntar')
	return HttpResponseRedirect('/administrador')
'''
Autor 			Jhonatan Acelas Arevalo
Fecha 	 		11 Julio 2015
Descripcion  	Renderiza la vista del login
Funcion 		Gestion.2
'''
def ingresar(request):
    return render(request, 'login.html', locals())
'''
Autor 			Jhonatan Acelas Arevalo
Fecha 	 		11 Julio 2015
Descripcion  	Recibe parametros del login,autentica el usuario en la db,
Funcion 		Gestion.3
'''
def logearse(request):
	if request.method == 'POST':
		formulario = AuthenticationForm(request.POST)
		if formulario.is_valid:
			usuario = request.POST['username']
			clave = request.POST['password']
			acceso = authenticate(username=usuario, password=clave)
			if acceso is not None:
				if acceso.is_active:
					login(request, acceso)
					return HttpResponseRedirect('/preguntar')
				else:
					return render_to_response('no_activo.html', context_instance=RequestContext(request))
			else:
				return HttpResponseRedirect('/administrador')
	return render_to_response('log.html',{'formulario':formulario}, context_instance=RequestContext(request))

'''
Autor 			Jhonatan Acelas Arevalo
Fecha 	 		11 Julio 2015
Descripcion  	Valida el tipo de usuario para cargar la correspondiente pagina inicial
Funcion 		Gestion.4
'''
@login_required(login_url='/logearse')
def preguntar(request):
	
	grupo = request.user.groups.all()[0].name 

	if grupo=='admin':
	 	return HttpResponseRedirect('/panelAdmin')
	return HttpResponse('no hay tipo usuario')
'''
Autor 			Jhonatan Acelas Arevalo
Fecha 	 		11 Julio 2015
Descripcion  	Finaliza session 
Funcion 		Gestion.5
'''
@login_required(login_url='/logearse')
def salir(request):
	logout(request)
	return HttpResponseRedirect('/')

@login_required(login_url='/logearse')
def baseadmin(request):
	reportado = Reported.objects.count()
	reportados = Reported.objects.all()

	return render(request, 'baseadmin.html', locals())

 

@login_required(login_url='/logearse')
def panelAdmin(request):
	posts = Post.objects.all().order_by('-id')
	lugares = Place.objects.all()
	reportado = Reported.objects.count()
	reportados = Reported.objects.all()
	 
	return render(request, 'plain_page.html', locals())

@login_required(login_url='/logearse')
def panelAdminFiltro(request,idLugar):
	try:
		place = Place.objects.get(pk=idLugar)
		posts = Post.objects.filter(place=place).order_by('-id')
		filtro = 'de ' + place.name
	except :
		pass
	lugares = Place.objects.all()

	reportado = Reported.objects.count()
	reportados = Reported.objects.all()


	 
	return render(request, 'plain_page.html', locals())




@login_required(login_url='/logearse')
def usuariosAdmin(request):
	profiles = Profile.objects.all()
	reportado = Reported.objects.count()
	reportados = Reported.objects.all()
	 
	return render(request, 'usuarios.html', locals())




@login_required(login_url='/logearse')
def formularioPublicar(request):
	lugares = Place.objects.all()
	reportado = Reported.objects.count()
	reportados = Reported.objects.all()
	 
	return render(request, 'formulario_publicar.html', locals())


@login_required(login_url='/logearse')
def guardarFoto(request):

	user_p = User.objects.get(username='boyacaen360@virtualscenes.co')
	profile = Profile.objects.get(user= user_p)
	post  =  Post(state='P' ,  user = profile)
	post.description = request.POST['description']
	post.type_post = request.POST['type_post']
	post.url_image = request.FILES['url_image']
	post.place = Place.objects.get(pk= request.POST['place'])
	post.save()


	lugares = Place.objects.all()
	mensaje = 'Se ha guardado la foto con Exito!'
	 
	return render(request, 'formulario_publicar.html', locals())
	#return redirect(formularioPublicar)


@login_required(login_url='/logearse')
def guardarVideo(request):

	user_p = User.objects.get(username='boyacaen360@virtualscenes.co')
	profile = Profile.objects.get(user= user_p)
	post  =  Post(state='P' ,  user = profile)

	post.description = request.POST['description']
	post.type_post = request.POST['type_post']
	post.url_video = request.POST['url_video']
	post.place = Place.objects.get(pk= request.POST['place'])
	post.save()


	lugares = Place.objects.all()
	mensaje = 'Se ha guardado el video con Exito!'
	 
	return render(request, 'formulario_publicar.html', locals())
	#return redirect(formularioPublicar)

@login_required(login_url='/logearse')
def eliminarPublicacion(request,idPublicacion):


	dell = Post.objects.get(pk=idPublicacion)
	dell.delete()

	posts = Post.objects.all().order_by('-id')
	reportado = Reported.objects.count()
	reportados = Reported.objects.all()

	mensaje = 'Se ha eliminado la publicacion'
	 
	return redirect(panelAdmin)



@login_required(login_url='/logearse')
def eliminarUsuario(request,idUsuario):


	dell = User.objects.get(pk=idUsuario)
	dell.delete()

	mensaje = 'Se ha eliminado la publicacion'
	 

	return redirect(usuariosAdmin)


@login_required(login_url='/logearse')
def reportadoAdmin(request):

	reportados = Reported.objects.all()

	return render(request, 'reportado.html', locals())

@login_required(login_url='/logearse')
def eliminarPost(request,idPost):

	dell = Post.objects.get(pk=idPost)
	dell.delete()
	mensaje = 'Se ha eliminado el post'

	return redirect(reportadoAdmin )

@login_required(login_url='/logearse')
def detalleusuario(request , idUsuario):
	#user = User.objects.get(pk= idUsuario)
	profile = Profile.objects.get(pk=idUsuario		)
	posts = Post.objects.filter(user = profile)
	return render(request, 'detalleusuario.html', locals())

