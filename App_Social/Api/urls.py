from django.conf.urls import url,include

from .import views






urlpatterns = [
    url(r'^index/', views.index, name='index'),
 
    url(r'^app/v1/api/gooauth/', views.gooauth, name='gooauth'),

    url(r'^app/v1/api/refreshYoutube/', views.refreshYoutube, name='refreshYoutube'),
    



]