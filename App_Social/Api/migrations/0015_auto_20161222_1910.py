# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-22 19:10
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Api', '0014_profile_id_facebook'),
    ]

    operations = [
        migrations.RenameField(
            model_name='like',
            old_name='user',
            new_name='profile',
        ),
    ]
